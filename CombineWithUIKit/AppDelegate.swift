//
//  AppDelegate.swift
//  CombineWithUIKit
//
//  Created by fatih on 20/01/2020.
//  Copyright © 2020 Fatih Kagan Emre. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {}
