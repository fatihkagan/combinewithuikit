//
//  SceneDelegate.swift
//  CombineWithUIKit
//
//  Created by fatih on 20/01/2020.
//  Copyright © 2020 Fatih Kagan Emre. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        
        let interactor = Interactor()
        let viewController = ViewController(interactor: interactor)
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
    }
}
