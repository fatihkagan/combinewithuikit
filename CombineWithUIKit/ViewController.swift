//
//  ViewController.swift
//  CombineWithUIKit
//
//  Created by fatih on 20/01/2020.
//  Copyright © 2020 Fatih Kagan Emre. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private let interactor: Interactor

    init(interactor: Interactor) {
         self.interactor = interactor
         super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("") }

    override func loadView() {
         let viewModel = interactor.viewModel()
         let mainView = MainView(viewModel: viewModel)
         self.view = mainView
    }
}
